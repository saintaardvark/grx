package main

import (
	"testing"
	// This may be worth looking at in the future
	// "github.com/rendon/testcli"
)

var LooksLikeLocalTests = []struct {
	target string
	result bool
}{
	{"foo", false},
	{"https://gitlab.com/saintaardvark/grx", true},
	{"git@gitlab.com:saintaardvark/grx", true},
}

func TestLooksLikeRepo(t *testing.T) {
	for _, tt := range LooksLikeLocalTests {
		t.Run(tt.target, func(t *testing.T) {
			answer := LooksLikeRepo(tt.target)
			if answer != tt.result {
				t.Errorf("%s: got %t, want %t", tt.target, answer, tt.result)
			}
		})
	}
}

var TidyTargetTests = []struct {
	target string
	result string
}{
	{"https://gitlab.com/saintaardvark/grx", "gitlab.com/saintaardvark/grx"},
	{"git@gitlab.com:saintaardvark/grx.git", "gitlab.com/saintaardvark/grx"},
	{"git@gitlab.com:PanosIs/polaris-data-client.git", "gitlab.com/PanosIs/polaris-data-client"},
	{"foo", "foo"},
	{"foo.bar/baz", "foo.bar/baz"},
}

func TestTidyTarget(t *testing.T) {
	for _, tt := range TidyTargetTests {
		t.Run(tt.target, func(t *testing.T) {
			answer := TidyTarget(tt.target)
			if answer != tt.result {
				t.Errorf("%s: got %s, want %s", tt.target, answer, tt.result)
			}
		})
	}
}

var TrimTrailingDotGitTests = []struct {
	dest   string
	result string
}{
	{"foo", "foo"},
	{"foo.git", "foo"},
	{"gitlab.com/PanosIs/polaris-data-client.git", "gitlab.com/PanosIs/polaris-data-client"},
}

func TestTrimTrailingDotGit(t *testing.T) {
	for _, tt := range TrimTrailingDotGitTests {
		t.Run(tt.dest, func(t *testing.T) {
			answer := TrimTrailingDotGit(tt.dest)
			if answer != tt.result {
				t.Errorf("%s: got %s, want %s", tt.dest, answer, tt.result)
			}
		})
	}
}

// Fixture to say directory always exists...
func dirCheckerAlwaysTrue(_ string) bool {
	return true
}

// ...or always say it doesn't.
func dirCheckerAlwaysFalse(_ string) bool {
	return false
}

var GuessDirTargetTests = []struct {
	devRoot       string
	target        string
	exists        func(string) bool
	desiredAnswer string
}{
	{
		"/home/aardvark/dev",
		"",
		dirCheckerAlwaysTrue,
		"/home/aardvark/dev/",
	}, // Empty string results in trailing slash, as we unconditionally append that
	{
		"/home/aardvark/dev",
		"foo",
		dirCheckerAlwaysTrue,
		"/home/aardvark/dev/foo",
	},
	{
		"/home/aardvark/dev",
		"foo/bar",
		dirCheckerAlwaysTrue,
		"/home/aardvark/dev/foo/bar",
	},
	{
		"/home/aardvark/dev",
		"foo/bar/",
		dirCheckerAlwaysTrue,
		"/home/aardvark/dev/foo/bar/",
	}, // We don't trim trailing slashes...might be worth reconsidering for consistency
	{
		"/home/aardvark/dev",
		"foo",
		dirCheckerAlwaysFalse,
		"foo",
	},
	{
		"/home/aardvark/dev",
		"foo/bar",
		dirCheckerAlwaysFalse,
		"foo/bar",
	},
	{
		"/home/aardvark/dev",
		"foo/bar/",
		dirCheckerAlwaysFalse,
		"foo/bar/",
	}, // We don't trim trailing slashes...might be worth reconsidering for consistency
}

func TestGuessDirTarget(t *testing.T) {
	for _, tt := range GuessDirTargetTests {
		t.Run(tt.target, func(t *testing.T) {
			answer := GuessDirTarget(tt.devRoot, tt.target, tt.exists)
			if answer != tt.desiredAnswer {
				t.Errorf("%s: got %s, want %s", tt.target, answer, tt.desiredAnswer)
			}
		})
	}
}

// // FIXME: hard-coding home directory for now
// func TestGrx(t *testing.T) {
// 	testcli.Run("./grx")
// 	if !testcli.Success() {
// 		t.Fatalf("Expected to succeed, but failed: %s", testcli.Error())
// 	}
// 	if !testcli.StdoutContains("/home/aardvark/dev") {
// 		t.Fatalf("Got %q, wanted %q", testcli.Stdout(), "/home/aardvark/dev")
// 	}
// }

var GetDevRootTests = []struct {
	input  string
	answer string
}{
	{"dev", "/home/aardvark/dev"},
	{"foo", "/home/aardvark/foo"},
	{"", "/home/aardvark/"},
	{"bar/baz", "/home/aardvark/bar/baz"},
	{"/dev/null", "/dev/null"},
}

func TestGetDevRoot(t *testing.T) {
	for _, tt := range GetDevRootTests {
		t.Run(tt.input, func(t *testing.T) {
			answer := GetDevRoot(tt.input)
			if answer != tt.answer {
				t.Errorf("%s: got %s, want %s", tt.input, answer, tt.answer)
			}
		})
	}
}
