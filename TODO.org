* Rules for how I want cd to work
  Assume dev dir == '/home/aardvark/dev/'
  - grx foo
    - checks for existence of /home/aardvark/dev/foo; if so, go there
    - checks for existence of /home/aardvark/dev/*/foo; if so, go there
    - checks for existence of /home/aardvark/dev/foo[something]; if
      so, go there
* TODO Allow partial matching of repo when cding
  Example:
  - assume path ~/dev/gitlab.com/saintaardvark/grx exists
  - "grx grx" should cd there
  - start by going to first match; future improvement could prompt for
    which one if partial match or multiple repos
* TODO Allow for configuration of devDir
* TODO Be able to pass args on to go get
  - at least "go get -u"
  - maybe with -- ?
* TODO Vendor deps
* TODO Consider use of go-getter
  - https://github.com/hashicorp/go-getter
* TODO Some kind of sanitization for URLs
   - cd $(grx example.com\;\ rm\ \-rf\ \*)
   - may not be much of a problem - URLs are pretty obvious
* DONE Runthrough in README
  CLOSED: [2019-03-14 Thu 21:04]
* DONE debug/loglevel switch [100%]
  CLOSED: [2019-03-16 Sat 07:38]
  - [X] Set default log level to error
  - [X] Add --debug switch
  - [X] Don't print out debug messages if log level is error
  - [X] Don't have --debug switch parsed as first arg (
    - eg: `grx --debug foo` should not try to cd to ~/dev/foo
* DONE Fall through/try first "go get"
  CLOSED: [2019-03-16 Sat 11:39]

* DONE Eliminate double-printout of debug messages
  CLOSED: [2019-03-16 Sat 11:39]
* DONE Allow for configuration of devDir
  CLOSED: [2019-04-05 Fri 05:48]
