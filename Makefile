build:
	go build .

test:
	go build -v && go test -v && go vet && golint && errcheck
