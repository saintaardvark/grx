package main

// grx: manage your dev directory like `go get`, but for everything

// Copyright (C) 2019 Hugh Brown (Saint Aardvark the Carpeted)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import (
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"strings"

	"github.com/op/go-logging"
	"github.com/urfave/cli"
)

var (
	copyright = `(c) 2019 Hugh Brown (Saint Aardvark the Carpeted)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
`
	debug           = false
	defaultLogLevel = logging.ERROR
	devDir          = "dev" // FIXME: Make customizable
	log             = logging.MustGetLogger("grx")
	loggingFormat   = logging.MustStringFormatter(
		`%{color}%{shortfunc} ▶ %{level:.6s} %{color:reset}%{message}`,
	)
)

func main() {
	setUpLogging(defaultLogLevel)
	app := cli.NewApp()
	app.Name = "grx"
	app.Version = "0.0.6"
	app.Copyright = copyright
	app.Usage = "manage your dev directory like `go get`, but for everything"
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "debug",
			Usage:       "debug",
			Destination: &debug,
		},
		cli.StringFlag{
			Name:        "dev-dir",
			Usage:       "Path to dev directory.  If not fully qualified, assumed to be under $HOME",
			Destination: &devDir,
			Value:       devDir,
			EnvVar:      "GRX_DEVDIR",
		},
	}
	app.Action = func(c *cli.Context) error {
		if debug == true {
			setUpLogging(logging.DEBUG)
			log.Debug("Turning on debug mode")
		}
		return handleTarget(c)
	}
	if err := app.Run(os.Args); err != nil {
		log.Errorf("Can't run application: %s", err.Error())
	}
}

func handleTarget(c *cli.Context) error {
	target := ""
	if len(c.Args()) == 0 {
		// Just cd to devRoot
		log.Debug("No args, so just dev root")
		target = GetDevRoot(devDir)
	} else {
		target = c.Args().Get(0)
		log.Debugf("Got args, so target now %s", target)
	}
	// does target look like a directory?
	if LooksLikeRepo(target) {
		log.Debugf("%s looks like repo to me", target)
		printDirForEval(HandleRepo(target))
		return nil
	}
	log.Debugf("%s looks like local directory to me", target)
	printDirForEval(GuessDirTarget(devDir, target, dirAlreadyExists))
	return nil
}

// GuessDirTarget checks if there's a match, whether full or partial,
// for this string
func GuessDirTarget(devRoot, dir string, dirChecker func(dir string) bool) string {
	dir = TidyTarget(dir)

	firstTry := fmt.Sprintf("%s/%s", GetDevRoot(devRoot), dir)
	log.Debugf("Checking for existence of %s", firstTry)
	if dirChecker(firstTry) {
		return firstTry
	}
	// Hail Mary
	lastChance := getDirectoriesGlob(devRoot)
	for _, target := range lastChance {
		target = fmt.Sprintf("%s/%s", devRoot, target)
		if candidate := loopThroughDirectories(target, dir); candidate != "" {
			return candidate
		}
		log.Debugf("Could not find match at %s", target)
	}
	log.Debug("Trying to see if we can find a partial match")
	if loopThroughDirectories(devRoot, dir) == "" {
		log.Debug("Could not find match at devRoot")
	}
	log.Debug("Shrug")
	return dir
}

// TidyTarget tries to change a (possible) URL into a directory name
func TidyTarget(target string) string {
	localDir := strings.Replace(target, "https://", "", 1)
	localDir = strings.Replace(localDir, "http://", "", 1)
	localDir = strings.Replace(localDir, "git@", "", 1)
	localDir = strings.Replace(localDir, ":", "/", 1)
	localDir = TrimTrailingDotGit(localDir)
	return localDir
}

// HandleRepo handles a repo (possibly by cloning it) and returns the
// path to its directory under devRoot
func HandleRepo(target string) string {
	// FIXME: Unsure how best to insert the dev root here.  If I leave it out of
	// GetLocalCopyDirectory, it makes it easier to test that function.
	localCopy := GetDevRoot(devDir) + "/" + TidyTarget(target)
	if dirAlreadyExists(localCopy) {
		return localCopy
	}
	log.Debug("Can't see local copy, trying go get")
	if err := tryGoGet(target); err != nil {
		log.Debugf("That didn't work: %s", err)
	}
	if err := tryCloning(target, localCopy); err != nil {
		log.Fatalf("Can't `go get` or `git clone` %s: %s", target, err.Error())
	}
	return localCopy
}

// LooksLikeRepo evaluates a string to see if it looks like a repo
// directory.  If it does, it returns True; if not, False.  NOTE: A
// better way to do this might be to use
// https://github.com/hashicorp/go-getter or something similar; for
// now, this works.
func LooksLikeRepo(target string) bool {
	if strings.HasPrefix(target, "http://") {
		return true
	} else if strings.HasPrefix(target, "https://") {
		return true
	} else if strings.Contains(target, "@") {
		return true
	}
	return false
}

// TrimTrailingDotGit removes, if present, ".git" from a string
func TrimTrailingDotGit(dest string) string {
	return strings.TrimSuffix(dest, ".git")
}

// printDirForEval prints the directory
// It's just a simple Println, but I want to make the intention clear.
func printDirForEval(dir string) {
	fmt.Println(dir)
}

func getDirectoriesGlob(root string) []string {
	answer := []string{}
	file, err := os.Open(root)
	if err != nil {
		log.Debugf("Could not open %s!", root)
	}
	answer, err = file.Readdirnames(-1)
	if err != nil {
		log.Debugf("Could not open %s!", root)
	}
	return answer
}

func loopThroughDirectories(root, target string) string {
	dirNames := getDirectoriesGlob(root)
	for _, directory := range dirNames {
		lastComponent := strings.Split(directory, "/")[0]
		if strings.HasPrefix(lastComponent, ".") {
			log.Debugf("Skipping %s because it starts with a dot", lastComponent)
			continue
		}
		nextTry := fmt.Sprintf("%s/%s/%s", root, directory, target)
		if dirAlreadyExists(nextTry) {
			log.Debugf("Found %s, going with that", nextTry)
			return nextTry
		}
	}
	return ""
}

// tryGoGet attempts to run `go get` on remote git repository
func tryGoGet(target string) error {
	log.Debugf("go get %s", target)
	cmd := exec.Command("go", "get", target)
	err := cmd.Run()
	return err
}

// tryCloning attempts to clone a remote git repository
func tryCloning(target, dest string) error {
	dest = TrimTrailingDotGit(dest)
	log.Debugf("git clone %s %s", target, dest)
	cmd := exec.Command("git", "clone", target, dest)
	err := cmd.Run()
	return err
}

// Configure logging.
// Thanks to @mashuiping
// (https://github.com/op/go-logging/issues/123#issuecomment-458879991)
// for a clear explanation.
func setUpLogging(level logging.Level) {
	backend := logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(backend, loggingFormat)
	logging.SetBackend(backendFormatter)
	logging.SetLevel(level, "grx")
}

// GetDevRoot returns the full path to the dev root directory as configured by the user
func GetDevRoot(devDir string) string {
	if IsFullPath(devDir) {
		return devDir
	}
	// Otherwise, assume that path needs to have $HOME prepended to it
	homeDir := os.Getenv("HOME")
	if homeDir == "" {
		usr, err := user.Current()
		if err != nil {
			log.Fatalf("Sorry, can't figure out your home directory: %s", err.Error())
		}
		homeDir = usr.HomeDir
	}
	return homeDir + "/" + devDir
}

// IsFullPath returns whether or not d is a fully-qualified path
func IsFullPath(d string) bool {
	return strings.HasPrefix(d, "/")
}

// FIXME: I'm sure this could be done better
func dirAlreadyExists(dir string) bool {
	log.Debugf("Checking if %s exists already", dir)
	stat, err := os.Stat(dir)
	if os.IsNotExist(err) {
		log.Debug("Nope, does not")
		return false
	}
	if stat == nil {
		log.Debug("stat looks nil, probably doesn't exist")
		return false
	}
	if stat.IsDir() == false {
		log.Debug("On second thought, doesn't look like directory after all. Never mind!")
	}
	log.Debug("Yes, it does")
	return true
}
