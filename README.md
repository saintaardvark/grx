# grx

Manages your dev directory like `go get`, but for everything, and with
the added feature of letting you change directory to that repo.

Given a url, or a directory name under $HOME/dev, grx will:

- clone that repo if needed and possible
- print out the full path to that directory, so that you can use it in
  a shell to change directory there

# How should I set up my dev directory?

```
ln -s $GOPATH/src dev
```

# What does that mean?

`go get` will clone a repository to $GOPATH/src/, with the full path
reflecting the URL you're cloning from.  `grx` aims to do the same,
but under $HOME/dev.  (That directory will be customizable in the
future, but for now it's hard-coded.)

Example: let's say you want to clone this repository.

```
./grx git@gitlab.com:saintaardvark/grx.git
Got args, so target now git@gitlab.com:saintaardvark/grx.git
20:26:02.542 handleTarget ▶ DEBUG 001  Got args, so target now git@gitlab.com:saintaardvark/grx.git
git@gitlab.com:saintaardvark/grx.git looks like repo to me
20:26:02.542 handleTarget ▶ DEBUG 002  git@gitlab.com:saintaardvark/grx.git looks like repo to me
Checking if /home/aardvark/dev/gitlab.com/saintaardvark/grx exists already
20:26:02.543 dirAlreadyExists ▶ DEBUG 003  Checking if /home/aardvark/dev/gitlab.com/saintaardvark/grx exists already
Nope, does not
20:26:02.543 dirAlreadyExists ▶ DEBUG 004  Nope, does not
git clone git@gitlab.com:saintaardvark/grx.git /home/aardvark/dev/gitlab.com/saintaardvark/grx
20:26:02.543 tryCloning ▶ DEBUG 005  git clone git@gitlab.com:saintaardvark/grx.git /home/aardvark/dev/gitlab.com/saintaardvark/grx
/home/aardvark/dev/gitlab.com/saintaardvark/grx
```

(Note: the logging will be customizable in the future, but for now is
hardcoded.)

Note that last line:

```
/home/aardvark/dev/gitlab.com/saintaardvark/grx
```

That is the directory it's been cloned to.  You could set up a shell
function like this:

```
# Bash:
h() { cd $(~/dev/grx/grx "$*") ;}
```

**However, this is not recommended right now:** there is *no* input
sanitation in here at all.  This is yet another feature I want to add.

But what if the project has already been cloned?  No worries, it will
detect that and just print out the directory it's been cloned to --
again, allowing you to use it in a shell function.

# TODO

See `TODO.org`.

# LICENSE

GPLv3; see ./LICENSE for details.
